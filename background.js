'use strict';

import { fetchSettings } from '/config/utils.js';

(() => {
	const setIcon = async () => {
		let settings = await fetchSettings();
		if (settings.length !== 0) {
			chrome.action.setIcon({
				path: "/icons/done.png"
			});
		}
	};

	chrome.runtime.onStartup.addListener(()=> {
		void setIcon();
	});
})()
