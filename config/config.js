﻿const config = {
	region: [
		{
			id: 1, vik_id: 15, title: 'Варна', sub_region: [
				{id: 1, vik_id: 16, title: 'Център'},
				{id: 2, vik_id: 17, title: 'Изток'},
				{id: 3, vik_id: 18, title: 'Запад'},
				{id: 4, vik_id: 19, title: 'Владиславово'},
				{id: 5, vik_id: 20, title: 'Аспарухово'},
				{id: 6, vik_id: 21, title: 'Аксаково'},
				{id: 7, vik_id: 22, title: 'Изгрев'}
			]
		},
		{
			id: 2, vik_id: 2, title: 'Девня', sub_region: [
				{id: 1, vik_id: 3, title: 'Суворово'},
				{id: 2, vik_id: 4, title: 'Девня'},
				{id: 3, vik_id: 5, title: 'Белослав'},
			]
		},
		{
			id: 3, vik_id: 6, title: 'Провадия', sub_region: [
				{id: 1, vik_id: 7, title: 'Вълчи дол'},
				{id: 2, vik_id: 8, title: 'Ветрино'},
				{id: 3, vik_id: 9, title: 'Провадия'},
				{id: 4, vik_id: 10, title: 'Дългопол'},
			]
		},
		{
			id: 4, vik_id: 11, title: 'Долни Чифлик', sub_region: [
				{id: 1, vik_id: 12, title: 'Аврен'},
				{id: 2, vik_id: 13, title: 'Долни Чифлик'},
				{id: 3, vik_id: 14, title: 'Бяла'},
			]
		},
	],
};
