export const fetchSettings = () => {
	return new Promise((resolve) => {
		chrome.storage.sync.get(['settings'], (obj) => {
			resolve(obj.settings ? JSON.parse(obj.settings) : []);
		});
	});
};