'use strict';

import {fetchSettings} from './config/utils.js';

window.onload = () => {
	console.log('the beginning');
	const formControl = document.querySelector(".form-control");
	const saveButton = document.querySelector('input[type="submit"]');
	const editSettings = document.querySelector("#settings");
	let settings = [];

	const setWorkIcon = () => {
		chrome.action.setIcon({
			path: "./icons/done.svg"
		});
	};

	const setSetupIcon = () => {
		chrome.action.setIcon({
			path: "./icons/setup.png"
		});
	};

	const createRegionSelect = () => {
		const region = document.querySelector('select[name="region"]');
		for (let i = 0; i < config.region.length; i++) {
			const option = document.createElement("option");
			option.value = config.region[i].id;
			option.text = config.region[i].title;
			region.appendChild(option);
		}

		return region;
	};

	const createSubRegionSelect = (subRegion, regionId) => {
		const subRegionList = config.region.find(region => region.id.toString() === regionId)?.sub_region;
		subRegion.classList.remove('hidden');
		subRegion.innerHTML = '';

		const option = document.createElement("option");
		option.value = '';
		option.text = '--';
		subRegion.appendChild(option);

		for (let i = 0; i < subRegionList.length; i++) {
			const option = document.createElement("option");
			option.value = subRegionList[i].id;
			option.text = subRegionList[i].title;
			subRegion.appendChild(option);
		}

		return subRegion;
	};

	const regionOptions = () => {
		const region = createRegionSelect();
		const subRegion = document.querySelector('select[name="sub_region"]');
		const address = document.querySelector('input[name="address"]');

		region.addEventListener('change', (event) => {
			event.preventDefault();
			const regionId = event.target.value;

			if (regionId === '') {
				subRegion.classList.add('hidden');
			} else {
				createSubRegionSelect(subRegion, regionId);
			}
		});

		subRegion.addEventListener('change', (event) => {
			event.preventDefault();
			const subRegionId = event.target.value;

			if (subRegionId === '') {
				saveButton.setAttribute('disabled', '');
				address.classList.add('hidden');
			} else {
				saveButton.removeAttribute('disabled');
				address.classList.remove('hidden');
			}
		});

		return region;
	};

	const getContentFromDiv = () => {
		return new Promise((resolve, reject) => {
			fetch('https://energo-pro.bg/bg/planirani-prekysvanija')
					.then(response => response.text())
					.then(html => {
						const parser = new DOMParser();
						const doc = parser.parseFromString(html, 'text/html');
						const div = doc.getElementsByClassName('interruption-data');
						if (div) {
							resolve(div.innerHTML);
						} else {
							console.log('reject');
							reject('Div with id "areas" not found');
						}
					})
					.catch(error => reject(error));
		});
	};

	const showSetupWindow = async () => {
		settings = await fetchSettings();

		if (settings.length === 0) {
			const select = regionOptions();
			formControl.insertBefore(select, formControl.firstChild);
			return;
		}

		setWorkIcon();
		formControl.classList.add('hidden');
		console.log(settings);
		const addressContainer = document.createElement('p');
		const region = config.region.find(region => region.id.toString() === settings.region);
		const sub_region = region.sub_region.find(sub_region => sub_region.id.toString() === settings.sub_region)
		const address = document.createTextNode(region.title + ', ' + sub_region.title + (settings.address !== '' ? ', ' + settings.address : ''));
		addressContainer.appendChild(address);
		editSettings.appendChild(addressContainer);

		const button = document.createElement("button");
		button.className = 'edit-settings';
		button.innerHTML = "смяна на адреса";
		button.addEventListener("click", function () {
			formControl.style.display = 'flex';
			const region = createRegionSelect();
			const subRegion = createSubRegionSelect(document.querySelector('select[name="sub_region"]'), settings.region);
			const address = document.querySelector('input[name="address"]');

			region.querySelector(`option[value="${settings.region}"]`).setAttribute("selected", "");
			subRegion.querySelector(`option[value="${settings.sub_region}"]`).setAttribute("selected", "");
			address.value = settings.address;
			address.classList.remove('hidden');
			address.classList.remove('hidden');

			button.classList.add('hidden');
			saveButton.removeAttribute('disabled');
			document.getElementById('settings').innerHTML = '';

			// chrome.storage.sync.remove(['settings']);
			// setSetupIcon();
		});
		editSettings.appendChild(button);
	};

	formControl.addEventListener('submit', (event) => {
		event.preventDefault();

		const formData = new FormData(formControl);
		const data = Object.fromEntries(formData);

		void chrome.storage.sync.set({
			['settings']: JSON.stringify(data)
		});

		window.location.reload();
	});

	void showSetupWindow();
};
